'use strict';

document.addEventListener('DOMContentLoaded', function() {
  headerScrollHandler.init();
  testimonialsCarouselHandler.init();
});

window.addEventListener('resize', function() {
  testimonialsCarouselHandler.calculateSize();
});