'use strict';

const mobileMenuHandler = {
  toggle() {
    $$('.header__menu__mobile-menu-icon')[0]
      .classList.toggle('_open');
    $$('.header__menu')[0]
      .classList.toggle('_open');
    $$('.body')[0]
      .classList.toggle('_no-scroll');
  }
};