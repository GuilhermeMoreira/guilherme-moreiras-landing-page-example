'use strict';

const testimonialsCarouselHandler = {
  data: {
    active_card: null,
    bullets: null,
    scrollContainer: null,
    cardWidth: null,
    arrowLeft: null,
    arrowRight: null,
    timeout: null
  },
  arrowUpdate(card) {
    if (card == 1) {
      this.data.arrowLeft.classList.add('_disabled');
    }
    if (card > 1) {
      this.data.arrowLeft.classList.remove('_disabled');
    }
    if (card < this.data.bullets.length) {
      this.data.arrowRight.classList.remove('_disabled');
    }
    if (card == this.data.bullets.length) {
      this.data.arrowRight.classList.add('_disabled');
    }
  },
  bulletUpdate(card) {
    for (var i = 0; i < this.data.bullets.length; i++) {
      this.data.bullets[i].classList.remove('_selected');
      if (i == this.data.bullets.length - 1) {
        this.data.bullets[card - 1].classList.add('_selected');
      }
    }
  },
  navigate(card) {
    if (card > this.data.bullets.length || card == 0) {
      return;
    }
    this.data.scrollContainer.scrollTo({
      left: (card - 1) * (this.data.cardWidth + 20),
      behavior: 'smooth'
    });
  },
  calculateSize() {
    const scrollArea = $$('.testimonials__cards-carousel__scroll-area')[0];
    this.data.cardWidth = $$('.testimonials__cards-carousel__card')[0].offsetWidth;
    scrollArea.style.width =  this.data.bullets.length * this.data.cardWidth + ($$('.testimonials__cards-carousel')[0].offsetWidth - this.data.cardWidth + 40) + 'px';
  },
  scroll(elem) {
    window.clearTimeout(this.data.timeout);
    this.data.timeout = window.setTimeout(() => {
      const cardInViewPort = Math.round(elem.scrollLeft/this.data.cardWidth + 1);
      if (cardInViewPort != this.data.active_card) {
        this.data.active_card = cardInViewPort;
        this.arrowUpdate(cardInViewPort);
        this.bulletUpdate(cardInViewPort);
      }
    }, 50);
  },
  init() {
    this.data.bullets = $$('.testimonials__cards-carousel__controls__bullets__bullet');
    this.data.scrollContainer = $$('.testimonials__cards-carousel')[0];
    this.data.arrowLeft = $$(`.testimonials__cards-carousel__controls__arrows__arrow._left`)[0];
    this.data.arrowRight = $$(`.testimonials__cards-carousel__controls__arrows__arrow._right`)[0];
    $$('.testimonials__cards-carousel')[0].addEventListener('scroll', function() {
      testimonialsCarouselHandler.scroll(this);
    });
    this.data.active_card = 1;
    this.calculateSize();
    this.navigate(1);
    this.arrowUpdate(1);
    this.bulletUpdate(1);
  }
};