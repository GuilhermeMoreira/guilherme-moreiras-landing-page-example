'use strict';

const headerScrollHandler = {
  data: {
    articleTopHeight: null,
    timeout: null
  },
  scrollCheck() {
    window.clearTimeout(this.data.timeout);
    this.data.timeout = window.setTimeout(() => {
      if (document.documentElement.scrollTop > this.data.articleTopHeight) {
        $$(`.body`)[0].classList.add('_fixedHeader');
        $$(`.header`)[0].classList.add('_fixed');
        setTimeout(function() {
          $$(`.header`)[0].classList.add('_visible');
        }, 100);
      } else {
        $$(`.header`)[0].classList.remove('_visible');
        setTimeout(function() {
          $$(`.body`)[0].classList.remove('_fixedHeader');
          $$(`.header`)[0].classList.remove('_fixed');
        }, 100);
      }
    }, 50);
  },
  init() {
    this.scrollCheck();
    this.data.articleTopHeight = $$(`.article-top`)[0].offsetHeight;
    document.addEventListener('scroll', function() {
      headerScrollHandler.scrollCheck();
    });
  }
};