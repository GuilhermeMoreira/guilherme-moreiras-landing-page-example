# Guilherme Moreira's Landing Page Example

## Dependencies:

- Node.js v10.22.1 or higher
- Gulp.js

## Usage:

- **npm install**
- **gulp serve** will serve at localhost on port 9000.
- The files **src/index.html**, **src/css/main.css** and **src/js/main.js** are generated solely for the purpose of serving on development environment. Do not edit those or they will be overwriten.
- **gulp build** will build for production.
- Have fun, and thanks for the oportunity :)
