const { series, parallel, src, dest, watch } = require('gulp');
const del = require('del');
const sass = require('gulp-sass');
const minify = require('gulp-clean-css');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify-es').default;
const htmlmin = require('gulp-htmlmin');
const connect = require('gulp-connect');
const hash_src = require('gulp-hash-src');
const fileinclude = require('gulp-file-include');

const srcPath = 'src';
const distPath = 'dist';

const clean = {
  dev() {
    return del([
      srcPath + '/css/*',
      srcPath + '/js/main.js',
    ]);
  },
  prod() {
    return del([
      distPath + '/img/*',
      distPath + '/css/*',
      distPath + '/js/*',
      '.tmp/*'
    ]);
  }
};

const css = {
  dev() {
    return src(srcPath + '/scss/*.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(dest(srcPath + '/css'));
  },
  prod() {
    return src(srcPath + '/scss/*.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(dest('.tmp/css'));
  },
  hash() {
    return src('.tmp/**/*.css')
      .pipe(minify({compatibility: 'ie9'}))
      .pipe(hash_src({build_dir: distPath, src_path: '.tmp'}))
      .pipe(dest(distPath));
  }
};

const js = {
  dev() {
    return src([
        srcPath + '/js/*.js',
        '!' + srcPath + '/js/main.js'
      ])
      .pipe(concat('main.js'))
      .pipe(dest(srcPath + '/js'))
      .pipe(connect.reload());
  },
  prod() {
    return src([
        srcPath + '/js/*.js',
        '!' + srcPath + '/js/main.js'
      ])
      .pipe(concat('main.js'))
      .pipe(dest('.tmp/js'))
      .pipe(src('.tmp/js/*.js'))
      .pipe(uglify())
      .pipe(dest(distPath + '/js'));
  }
};

const includes = function() {
  return src(srcPath + '/views/*.html')
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(dest(srcPath));
};

const html = {
  minify() {
    return src(srcPath + '/*.html')
      .pipe(htmlmin({ collapseWhitespace: true }))
      .pipe(dest('.tmp'));
  },
  hash() {
    return src('.tmp/*.html')
      .pipe(htmlmin())
      .pipe(hash_src({build_dir: distPath, src_path: '.tmp'}))
      .pipe(dest(distPath));
  }
};

const img = function() {
  return src(srcPath + '/img/**/*.*')
    .pipe(dest(distPath + '/img'));
};

const fonts = function() {
  return src(srcPath + '/fonts/*.*')
    .pipe(dest(distPath + '/fonts'));
};

const watcher = function() {
  watch([
    srcPath + '/views/**/*.html',
    srcPath + '/img/*.*',
    srcPath + '/scss/**/*.scss',
    srcPath + '/js/*.js',
    '!' + srcPath + '/js/main.js',
  ], { delay: 500 }, series(css.dev, js.dev, includes));
};

const connector = function() {
  return connect.server({
      root: 'src',
      port: 9000,
      livereload: true
    });
};

exports.serve = series(clean.dev, css.dev, js.dev, includes, parallel(connector, watcher));

exports.build = series(clean.prod, includes, parallel(css.prod, js.prod, html.minify, img, fonts), css.hash, html.hash);
